package app_login

import (
	"net/http"

	"gitlab.com/spacetimi/passman/passman_server/app_src/app_routes"
	"gitlab.com/spacetimi/passman/passman_server/app_src/login"
	"gitlab.com/spacetimi/shared/timi_shared_server/code/core/controller"
)

func (alh *AppLoginHandler) handleLogout(httpResponseWriter http.ResponseWriter,
	request *http.Request,
	args *controller.HandlerFuncArgs) {

	login.LogoutUser(httpResponseWriter)
	http.Redirect(httpResponseWriter, request, app_routes.Login, http.StatusSeeOther)
}
